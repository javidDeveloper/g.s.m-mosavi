package developer.javid.ir.mosavi.Model;

/**
* Developed by javidDeveloper.ir  *   */
public class Message {
    private String id;
    private String title;
    private String text;
    private String link;
    private String pic;
    private String date;

    public Message(String id, String title, String text, String link, String pic, String date) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.link = link;
        this.pic = pic;
        this.date = date;
    }

    public Message() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
