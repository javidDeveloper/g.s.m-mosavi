package developer.javid.ir.mosavi.Manager;

import developer.javid.ir.mosavi.R;
import developer.javid.ir.mosavi.Tools.ContextModel;

/**
 * Developed by javidDeveloper.ir  *
 */
public class ColorManager {
    public static ColorManager instance;
    /****************** color *************************/
    public static final String ERROR_TEXT_COLOR = "errorTextColor";
    public static final String TEXT_COLOR = "text_color";
    public static final String COLOR_PRIMARY = "color_primary";
    public static final String COLOR_PRIMARY_DARK = "color_primary_dark";
    public static final String COLOR_ACCENT = "color_accent";
    public static final String TOOLBAR_TEXT_COLOR = "toolbar_text_color";
    public static final String TEXT_COLOR_DARK = "text_color_dark";
    public static final String TEXT_COLOR_STATUS_SUCCESS = "text_color_status_success";
    public static final String TEXT_COLOR_STATUS_FAILED = "text_color_status_failed";
    public static final String TAB_ICON = "tab_icon";
    public static final String WHITE = "white";

    public static ColorManager getInstance() {
        if (instance == null)
            instance = new ColorManager();
        return instance;
    }

    public int getColorByName(String colorName) {
        String color = colorName;
        switch (color) {
            case ERROR_TEXT_COLOR:
                return ContextModel.getContext().getResources().getColor(R.color.red);
            case TOOLBAR_TEXT_COLOR:
                return ContextModel.getContext().getResources().getColor(R.color.toolbar_text_color);
            case TEXT_COLOR:
                return ContextModel.getContext().getResources().getColor(R.color.text_color);
            case WHITE:
                return ContextModel.getContext().getResources().getColor(R.color.white);
            case TAB_ICON:
                return ContextModel.getContext().getResources().getColor(R.color.tab_icon);
            case TEXT_COLOR_DARK:
                return ContextModel.getContext().getResources().getColor(R.color.text_color_dark);
            case TEXT_COLOR_STATUS_SUCCESS:
                return ContextModel.getContext().getResources().getColor(R.color.text_color_status_success);
            case TEXT_COLOR_STATUS_FAILED:
                return ContextModel.getContext().getResources().getColor(R.color.text_color_status_failed);
            case COLOR_PRIMARY:
                return ContextModel.getContext().getResources().getColor(R.color.colorPrimary);
            case COLOR_PRIMARY_DARK:
                return ContextModel.getContext().getResources().getColor(R.color.colorPrimaryDark);
            case COLOR_ACCENT:
                return ContextModel.getContext().getResources().getColor(R.color.colorAccent);
            default:
                break;
        }
        return 0;
    }


}
