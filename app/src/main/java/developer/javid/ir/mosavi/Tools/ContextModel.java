package developer.javid.ir.mosavi.Tools;

import android.app.Activity;
import android.content.Context;

/**
* Developed by javidDeveloper.ir  *   */
public class ContextModel {
    private static Context context;
    private static Activity currentActivity;

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        ContextModel.context = context;
    }

    public static Activity getCurrentActivity() {
        return currentActivity;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        ContextModel.currentActivity = currentActivity;
    }
}
