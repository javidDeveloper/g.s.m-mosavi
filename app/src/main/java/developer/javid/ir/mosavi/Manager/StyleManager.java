package developer.javid.ir.mosavi.Manager;

import android.widget.ImageView;
import android.widget.TextView;

/**
* Developed by javidDeveloper.ir  *   */
public class StyleManager {
    public static StyleManager instance;

    public static StyleManager getInstance() {
        if (instance == null)
            instance = new StyleManager();
        return instance;
    }

    public void changeTintColor(ImageView imageView, String colorName) {
        imageView.setColorFilter(ColorManager.getInstance().getColorByName(colorName), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    public void changeTextColor(TextView textView, String colorName) {
        textView.setTextColor(ColorManager.getInstance().getColorByName(colorName));
    }
}
