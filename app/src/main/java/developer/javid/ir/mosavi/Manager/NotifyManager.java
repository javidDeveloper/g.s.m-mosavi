//package developer.javid.ir.mosavi.Manager;
//
//import android.app.Notification;
//import android.app.NotificationChannel;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.support.v4.app.NotificationCompat;
//
//import com.orhanobut.hawk.Hawk;
//
//import developer.javid.ir.mosavi.Model.Message;
//import developer.javid.ir.mosavi.Model.MessageList;
//import developer.javid.ir.mosavi.R;
//import developer.javid.ir.mosavi.Tools.ContextModel;
//import developer.javid.ir.mosavi.Tools.NotificationReceiver;
//
///**
//* Developed by javidDeveloper.ir
// */
//public class NotifyManager {
//    public static NotifyManager instance;
//    private CharSequence name = "پیامک ساز";
//    private NotificationManager mNotificationManager;
//
//    public static NotifyManager getInstance() {
//        if (instance == null)
//            instance = new NotifyManager();
//        return instance;
//    }
//
//    public NotificationManager getmNotificationManager() {
//        return mNotificationManager;
//    }
//
//    public void showNotification(MessageList messages) {
//        for (Message message : messages.getMessages()) {
//            if (!Hawk.contains(message.getId())) {
//                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//                    int importance = NotificationManager.IMPORTANCE_HIGH;
//                    Intent intent = new Intent(ContextModel.getContext(), NotificationReceiver.class);
//                    intent.setAction(message.getId());
//                    PendingIntent pendingIntent = PendingIntent.getBroadcast(ContextModel.getContext(), 0, intent, 0);
//                    NotificationChannel mChannel = new NotificationChannel(message.getId(), name, importance);
//                    Notification notification = new Notification.Builder(ContextModel.getContext())
//                            .setContentTitle(message.getTitle())
//                            .setContentText(message.getText())
//                            .setSmallIcon(R.drawable.ic_launcher_foreground)
//                            .setChannelId(message.getId())
//                            .setContentIntent(pendingIntent)
//                            .build();
//                    mNotificationManager =
//                            (NotificationManager) ContextModel.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
//                    mNotificationManager.createNotificationChannel(mChannel);
//                    mNotificationManager.notify(Integer.parseInt(message.getId()), notification);
//
//                } else /*if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O)*/ {
//                    Intent intent = new Intent(ContextModel.getContext(), NotificationReceiver.class);
//                    intent.setAction(message.getId());
//                    PendingIntent pendingIntent = PendingIntent.getBroadcast(ContextModel.getContext(), 0, intent, 0);
//                    NotificationCompat.Builder mBuilder =
//                            new NotificationCompat.Builder(ContextModel.getContext())
//                                    .setSmallIcon(R.drawable.ic_launcher_foreground)
//                                    .setContentTitle(message.getTitle())
//                                    .setContentText(message.getText())
//                                    .setContentIntent(pendingIntent);
//                    mNotificationManager =
//                            (NotificationManager) ContextModel.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
//                    mNotificationManager.notify(Integer.parseInt(message.getId()), mBuilder.build());
//                }
//                Hawk.put(message.getId(), message.getId());
//            }
//        }
//    }
//
//
//}
