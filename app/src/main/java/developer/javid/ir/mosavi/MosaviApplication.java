package developer.javid.ir.mosavi;

import android.app.Application;
import android.app.Fragment;
import android.transition.Transition;

import com.orhanobut.hawk.Hawk;

import developer.javid.ir.mosavi.Tools.ContextModel;


/**
* Developed by javidDeveloper.ir
 */
public class MosaviApplication extends Application {
    private static MosaviApplication instance;
//    private static Fragment fragment;
//    private static Transition transaction;
    @Override
    public void onCreate() {
        super.onCreate();
        ContextModel.setContext(getApplicationContext());
        Hawk.init(getApplicationContext()).build();
        instance = this;
//        OneSignal.startInit(this)
//                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
//                .unsubscribeWhenNotificationsAreDisabled(true)
//                .init();
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
