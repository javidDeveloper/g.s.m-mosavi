package developer.javid.ir.mosavi;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;
import com.marcoscg.dialogsheet.DialogSheet;
import java.util.Observable;
import java.util.Observer;
import developer.javid.ir.mosavi.Fragmnets.AboutMeFragment;
import developer.javid.ir.mosavi.Fragmnets.CalculateFragment;
import developer.javid.ir.mosavi.Fragmnets.CallMeFragment;
import developer.javid.ir.mosavi.Fragmnets.SettingFragment;
import developer.javid.ir.mosavi.Manager.ConfigManager;
import developer.javid.ir.mosavi.Manager.SettingManager;
import developer.javid.ir.mosavi.Model.VersionControl;
import developer.javid.ir.mosavi.Tools.ContextModel;
import developer.javid.ir.mosavi.Tools.Utility;
import developer.javid.ir.mosavi.Widgets.CustomButton;
import developer.javid.ir.mosavi.Widgets.CustomTextView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Observer {
    private DialogSheet dialogSheet;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private Fragment fragment;
    private FragmentTransaction transaction;
    private Context context = MainActivity.this;
    private VersionControl versionControl;
    private Dialog dialogUp = new Dialog(ContextModel.getContext());
    private CustomTextView tvVersion;

    public NavigationView getNavigationView() {
        return navigationView;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dialogUp.isShowing()) {
            dialogUp.dismiss();
        }
//        NetworkManager.getInstance().callVersion();
    }

    public DrawerLayout getDrawer() {
        return drawer;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ContextModel.setCurrentActivity(this);
        initViews();
        Utility.hideKeyboard();
        dialogSheet = new DialogSheet(context);
        navigationView.setNavigationItemSelectedListener(this);
        tvVersion.setText(Utility.versionName());
        if (findViewById(R.id.frame_continer) != null) {
            if (savedInstanceState != null) {
                return;
            }
            fragment = new CalculateFragment();
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_continer, fragment);
            transaction.commit();
            navigationView.setCheckedItem(R.id.nav_calculate);
        }
        if (SettingManager.getInstance().getFirstRun()) {
            new CountDownTimer(2000, 1000) {

                public void onTick(long millisUntilFinished) {

                }

                public void onFinish() {
                    drawer.openDrawer(Gravity.RIGHT);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            drawer.closeDrawer(Gravity.RIGHT);
                            SettingManager.getInstance().setFirstRun(false);
                        }
                    }, 2000);
                }
            }.start();
        }
    }


    private void initViews() {
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        tvVersion = headerView.findViewById(R.id.tv_version);

    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
//        if (id == R.id.nav_home) {
//            fragment = new FirstFragment();
//            transaction = getSupportFragmentManager().beginTransaction();
//            transaction.replace(R.id.frame_continer, fragment);
//            transaction.addToBackStack(null);
//            transaction.commit();
//        }
        if (id == R.id.nav_calculate) {
            navigationView.setCheckedItem(R.id.nav_calculate);
            fragment = new CalculateFragment();
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_continer, fragment);
            transaction.commit();
        } else if (id == R.id.nav_call_me) {
            navigationView.setCheckedItem(R.id.nav_call_me);
            fragment = new CallMeFragment();
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_continer, fragment);
            transaction.commit();
            navigationView.setCheckedItem(R.id.nav_setting);
        } else if (id == R.id.nav_setting) {
            navigationView.setCheckedItem(R.id.nav_product_me);
            fragment = new SettingFragment();
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_continer, fragment);
            transaction.commit();
        } else if (id == R.id.nav_product_me) {
            navigationView.setCheckedItem(R.id.nav_product_me);
            fragment = new AboutMeFragment();
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_continer, fragment);
            transaction.commit();
            navigationView.setCheckedItem(R.id.nav_product_me);
        } else if (id == R.id.nav_telegram) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.telegram_link)));
            startActivity(browserIntent);
        } else if (id == R.id.nav_instagram) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.instagram_link)));
            startActivity(browserIntent);
        }
        drawer.closeDrawer(Gravity.RIGHT);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            if (drawer.isDrawerOpen(Gravity.RIGHT)) {
                drawer.closeDrawer(Gravity.RIGHT);
            } else {
                showDialogExit();
            }
        } else {
            super.onBackPressed();
        }

    }

    private void showDialogExit() {
        dialogSheet.setTitle(R.string.app_name);
        dialogSheet.setMessage(R.string.exit_message);
        dialogSheet.setIcon(ContextModel.getContext().getResources().getDrawable(R.mipmap.ic_logo));
        dialogSheet.setCancelable(true);
        dialogSheet.setPositiveButton(ConfigManager.getMarket() == ConfigManager.market.bazzar ? R.string.submit_comment_bazzar : R.string.submit_comment_myket, new DialogSheet.OnPositiveClickListener() {
            @Override
            public void onClick(View v) {
                if (ConfigManager.getMarket() == ConfigManager.market.bazzar) {
                    try {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(getString(R.string.commment_bazar)));
                        startActivity(intent);
                    } catch (Exception e) {
                        Toast.makeText(ContextModel.getContext(), "برنامه کافه بازار یافت نشد!!", Toast.LENGTH_SHORT).show();
                    }
                } else if (ConfigManager.getMarket() == ConfigManager.market.myKet) {
                    try {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(getString(R.string.comment_myket)));
                        startActivity(intent);
                    } catch (Exception e) {
                        Toast.makeText(context, "برنامه مایکت یافت نشد!!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "شما از نسخه آزاد استفاده میکنید", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialogSheet.setNegativeButton(R.string.exit, new DialogSheet.OnNegativeClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        dialogSheet.setBackgroundColor(context.getResources().getColor(R.color.white));
        dialogSheet.setButtonsColorRes(R.color.colorPrimary);
        dialogSheet.show();
    }

    private void versionControler(VersionControl o) {
        this.versionControl = o;
        if (ConfigManager.getInstance().needUpdate(o)) {
            showDialogUpdate();
        }
    }

    private void showDialogUpdate() {
        dialogUp = new Dialog(context);
        dialogUp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogUp.setContentView(R.layout.dialog_update_version);
        final CustomButton upadteButton = dialogUp.findViewById(R.id.button_update);
        upadteButton.setText(String.format("بروزرسانی در %s", ConfigManager.getMarketName()));
        upadteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ConfigManager.getAppUpdate()));
                startActivity(browserIntent);
            }
        });

        if (versionControl.getNeedUpdate() == 1)
            dialogUp.setCancelable(false);
        dialogUp.show();
    }

    @Override
    public void update(Observable observable, Object o) {
//        if (observable instanceof NetworkManager) {
//            if (o instanceof MessageList) {
//                NotifyManager.getInstance().showNotification((MessageList) o);
//            }
//            if (o instanceof VersionControl) {
        versionControler((VersionControl) o);
//            }
//        }
    }

}
