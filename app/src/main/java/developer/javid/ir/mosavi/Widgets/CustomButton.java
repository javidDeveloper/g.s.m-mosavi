package developer.javid.ir.mosavi.Widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;


public class CustomButton extends AppCompatButton {

    public CustomButton(Context context) {
        super(context);
        typeFont();
        setEnable();
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        typeFont();
        setEnable();
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void typeFont() {
        Typeface face = Typeface.createFromAsset(getContext().getAssets(),
                "font/Gandom.ttf");
        setTypeface(face);
    }

    public void setEnable() {
        if (isEnabled()) {
//            setBackground(getResources().getDrawable(R.drawable.button_enable));
//            setTextColor(getResources().getColor(R.color.text_color_button_enable));
//        } else {
//            setBackground(getResources().getDrawable(R.drawable.button_disable));
//            setTextColor(getResources().getColor(R.color.text_color_button_disable));
//        }
        }
    }
}
