package developer.javid.ir.mosavi.Widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import developer.javid.ir.mosavi.Manager.StyleManager;
import developer.javid.ir.mosavi.R;


public class CustomTextView extends AppCompatTextView {
    private Typeface face;

    public CustomTextView(Context context) {
        super(context);
        typeFont();
        initView(context, null);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        typeFont();
        initView(context, attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        typeFont();
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        TypedArray colorAttr = context.obtainStyledAttributes(attrs, R.styleable.CustomView);
        String colorName = colorAttr.getString(R.styleable.CustomView_viewColorName);
        if (colorName == null || colorName.equals("")) {

        } else {
            StyleManager.getInstance().changeTextColor(this,colorName);
        }
    }

    public void typeFont() {
        face = Typeface.DEFAULT;
        if (face.isBold()) {
            face = Typeface.createFromAsset(getContext().getAssets(), "font/Gandom-Bold.ttf");
            setTypeface(face);
        } else if (face.isItalic()) {
            face = Typeface.createFromAsset(getContext().getAssets(), "font/Gandom.ttf");
            setTypeface(face);
        } else {
            face = Typeface.createFromAsset(getContext().getAssets(), "font/Gandom.ttf");
            setTypeface(face);
        }
    }
}
