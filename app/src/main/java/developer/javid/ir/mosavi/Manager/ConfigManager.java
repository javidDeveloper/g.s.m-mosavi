package developer.javid.ir.mosavi.Manager;

import developer.javid.ir.mosavi.Model.VersionControl;
import developer.javid.ir.mosavi.R;
import developer.javid.ir.mosavi.Tools.ContextModel;
import developer.javid.ir.mosavi.Tools.Utility;

/**
 * Developed by javidDeveloper.ir
 */
public class ConfigManager {

    private String CURRENT_VERSION = Utility.versionName();
    public static ConfigManager instance;
    public static market config;

    public static ConfigManager getInstance() {
        if (instance == null)
            instance = new ConfigManager();
        return instance;
    }

    public enum market {
        bazzar, myKet, azad
    }

    public static ConfigManager.market getMarket() {
        config = market.bazzar;
        return config;
    }

    public static String getMarketVersionName() {
        switch (getMarket()) {
            case azad:
                return ContextModel.getContext().getResources().getString(R.string.azad_version);
            case bazzar:
                return ContextModel.getContext().getResources().getString(R.string.bazzar_version);
            case myKet:
                return ContextModel.getContext().getResources().getString(R.string.myKet_version);
        }
        return "";
    }

    public static String getMarketName() {
        switch (getMarket()) {
            case azad:
                return ContextModel.getContext().getResources().getString(R.string.azad);
            case bazzar:
                return ContextModel.getContext().getResources().getString(R.string.bazzar);
            case myKet:
                return ContextModel.getContext().getResources().getString(R.string.myKet);
        }
        return "";
    }

    public static String getAppUpdate() {
        switch (getMarket()) {
            case azad:
                return ContextModel.getContext().getResources().getString(R.string.update_link_bazzar);
            case bazzar:
                return ContextModel.getContext().getResources().getString(R.string.update_link_bazzar);
            case myKet:
                return ContextModel.getContext().getResources().getString(R.string.update_link_myket);
        }
        return "";
    }

    public boolean needUpdate(VersionControl versionControl) {
        if (versionControl == null) {
            return false;
        } else if (!versionControl.getVersion().equals(CURRENT_VERSION)) {
            return true;
        }
        return false;
    }


}
