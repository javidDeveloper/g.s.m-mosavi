package developer.javid.ir.mosavi.Widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import developer.javid.ir.mosavi.Manager.StyleManager;
import developer.javid.ir.mosavi.R;
import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class CustomImageView extends AppCompatImageView {
    public CustomImageView(Context context) {
        super(context);
        initView(context, null);
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);

    }

    private void initView(Context context, AttributeSet attrs) {
        TypedArray colorAttr = getContext().obtainStyledAttributes(attrs, R.styleable.CustomView);
        String colorName = colorAttr.getString(R.styleable.CustomView_viewColorName);
        if (colorName == null || colorName.equals("")) {

        } else {
            StyleManager.getInstance().changeTintColor(this,colorName);
        }
    }

    public void Load(Drawable url) {
        Glide.with(this).load(url).into(this);
    }

    public void LoadBlure(String url, int transform) {
        Glide.with(this)
                .load(url)
                .apply(bitmapTransform(new BlurTransformation(transform)))
                .into(this);

    }

    public void LoadCircle(String url) {
        Glide.with(this)
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .into(this);

    }
}
