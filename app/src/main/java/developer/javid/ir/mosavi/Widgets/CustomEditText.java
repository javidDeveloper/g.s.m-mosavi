package developer.javid.ir.mosavi.Widgets;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public class CustomEditText extends AppCompatEditText {
    private Typeface face;

    public CustomEditText(Context context) {
        super(context);
        typeFont();
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        typeFont();
    }


    public void typeFont(){
        face = Typeface.DEFAULT;
        if(face.isBold()){
            face = Typeface.createFromAsset(getContext().getAssets(),"font/Gandom-Boldttf");
            setTypeface(face);
        }else if(face.isItalic()){
            face = Typeface.createFromAsset(getContext().getAssets(),"font/Gandom.ttf");
            setTypeface(face);
        }else{
            face = Typeface.createFromAsset(getContext().getAssets(),"font/Gandom.ttf");
            setTypeface(face);
        }
    }
}
