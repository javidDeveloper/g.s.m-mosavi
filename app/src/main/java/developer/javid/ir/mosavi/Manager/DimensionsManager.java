package developer.javid.ir.mosavi.Manager;

import developer.javid.ir.mosavi.R;
import developer.javid.ir.mosavi.Tools.ContextModel;
import developer.javid.ir.mosavi.Tools.Utility;

/**
 * Developed by javidDeveloper.ir
 */
public class DimensionsManager {
    private static DimensionsManager instance;

    public static DimensionsManager getInstance() {
        if (instance == null)
            instance = new DimensionsManager();
        return instance;
    }

    public int heightFrame(double mainHeight) {
        return (int) (mainHeight - 6);
    }

    public int widthFrame(double mainWidth) {
        return (int) (mainWidth - 6);
    }

    public double movable(double mainHeight) {
        return mainHeight - 7.5;
    }

    public double heightNet(double mainHeight) {
        return mainHeight - 3.5;
    }

    public double stepNet(double mainWidth) {
        return (mainWidth * 45) / 100;
    }

    public int thread(double mainHeight, double mainWidth) {
        return (int) (mainHeight + mainWidth + 25);
    }


    public String totalSquareMeter(double mainHeight, double mainWidth) {
        double xM = mainWidth / 100;
        double yM = mainHeight / 100;
        double f = xM * yM;
        int n = 3;
        double d = f * Math.pow(10, n);
        double a = Math.nextUp(d);
        double result = (int) a / Math.pow(10, n);
        if (result <= 1)
            return String.valueOf(1);
        else return String.valueOf(result);
    }

    public String totalPrice(double mainHeight, double mainWidth, long price) {
        double result = 0;
        if (SettingManager.getInstance().getUnitPrice().equals(SettingManager.TOMAN)) {
            result = (int) (mainHeight * mainWidth * price) / 10000;
            return Utility.formatNumbers(result) + " " + ContextModel.getContext().getString(R.string.toman);
        } else if (SettingManager.getInstance().getUnitPrice().equals(SettingManager.RIAL)) {
            result = (int) (mainHeight * mainWidth * price) / 10000;
            return Utility.formatNumbers(result) + " " + ContextModel.getContext().getString(R.string.rial);
        }
        return " ";

    }

}
