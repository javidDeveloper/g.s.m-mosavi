//package developer.javid.ir.mosavi.Fragmnets.Adapters;
//
//import android.content.Context;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//
//import developer.javid.ir.mosavi.Tools.OnItemClickListener;
//
//
///**
//* Developed by javidDeveloper.ir  *   */
//public class FastAccessAdapter extends RecyclerView.Adapter<FastAccessAdapter.ViewHolder> {
//    List<FastAccess> list;
//    Context context;
//    View rootView;
//    OnItemClickListener onItemClickListener;
//
//
//    public List<FastAccess> getList() {
//        return list;
//    }
//
//    public void setList(List<FastAccess> list) {
//        this.list = list;
//    }
//
//    public OnItemClickListener getOnItemClickListener() {
//        return onItemClickListener;
//    }
//
//    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
//        this.onItemClickListener = onItemClickListener;
//    }
//
//    public FastAccessAdapter(List<FastAccess> list, Context context) {
//        this.list = list;
//        this.context = context;
//    }
//
//    @NonNull
//    @Override
//    public FastAccessAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        rootView = LayoutInflater.from(context).inflate(R.layout.fast_access_item, parent, false);
//        return new ViewHolder(rootView);
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull FastAccessAdapter.ViewHolder holder, final int position) {
//        holder.imageViewAvatar.Load(list.get(position).getAvatar());
//        holder.textViewFastAccess.setText(list.get(position).getTitle());
//        holder.linearLayoutFastAccess.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onItemClickListener.onItemClicked(position);
//            }
//        });
//    }
//
//    @Override
//    public int getItemCount() {
//        return list == null ? 0 : list.size();
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder {
//        LinearLayout linearLayoutFastAccess;
//        CustomImageView imageViewAvatar;
//        CustomTextView textViewFastAccess;
//
//        public ViewHolder(View itemView) {
//            super(itemView);
//            linearLayoutFastAccess = itemView.findViewById(R.id.linearLayout_fast_access);
//            imageViewAvatar = itemView.findViewById(R.id.imageView_avatar);
//            textViewFastAccess = itemView.findViewById(R.id.textView_fast_access);
//        }
//    }
//}
