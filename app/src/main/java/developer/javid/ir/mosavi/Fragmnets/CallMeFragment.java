package developer.javid.ir.mosavi.Fragmnets;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import developer.javid.ir.mosavi.MainActivity;
import developer.javid.ir.mosavi.Manager.SettingManager;
import developer.javid.ir.mosavi.R;
import developer.javid.ir.mosavi.Tools.Utility;
import developer.javid.ir.mosavi.Widgets.CustomButton;
import developer.javid.ir.mosavi.Widgets.CustomImageView;
import developer.javid.ir.mosavi.Widgets.CustomTextView;


/**
 * Developed by javidDeveloper.ir  *
 */
public class CallMeFragment extends Fragment {
    private View rootView;
    private RelativeLayout rl_call_one;
    private RelativeLayout rl_call_tow;
    private RelativeLayout rl_call_three;
    private RelativeLayout rl_call_four;
    private CustomTextView txt_call_one;
    private CustomTextView txt_call_tow;
    private CustomTextView txt_call_three;
    private CustomTextView txt_call_four;
    private CustomImageView imageViewMenu;
    private CustomTextView t2;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_call_me, container, false);
        initViews();
        txt_call_one.setText(Utility.convertNumbersToPersian("32318048 - 026" + " " + ""));
        txt_call_tow.setText(Utility.convertNumbersToPersian("09122807860" + " " + "(موسوی)"));
        txt_call_three.setText(Utility.convertNumbersToPersian("09122025507" + " " + "(موسوی)"));
        txt_call_four.setText(Utility.convertNumbersToPersian("09123665598" + " " + "(موسوی)"));
        t2.setText(Utility.newLine(Utility.convertNumbersToPersian(getResources().getString(R.string.address))));

        rl_call_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.callNumber(getContext(), "02632318048");
            }
        });
        rl_call_tow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.callNumber(getContext(), "09122807860");
            }
        });
        rl_call_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.callNumber(getContext(), "09122025507");
            }
        });
        rl_call_four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.callNumber(getContext(), "09123665598");
            }
        });
        imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getContext()).getDrawer().openDrawer(Gravity.RIGHT);
            }
        });
        return rootView;
    }


    private void initViews() {
        imageViewMenu = rootView.findViewById(R.id.imageView_menu);
        rl_call_one = rootView.findViewById(R.id.rl_call_one);
        rl_call_tow = rootView.findViewById(R.id.rl_call_tow);
        rl_call_three = rootView.findViewById(R.id.rl_call_three);
        rl_call_four = rootView.findViewById(R.id.rl_call_four);
        txt_call_one = rootView.findViewById(R.id.txt_call_one);
        txt_call_tow = rootView.findViewById(R.id.txt_call_tow);
        txt_call_three = rootView.findViewById(R.id.txt_call_three);
        txt_call_four = rootView.findViewById(R.id.txt_call_four);
        t2 = rootView.findViewById(R.id.t2);
    }
}
