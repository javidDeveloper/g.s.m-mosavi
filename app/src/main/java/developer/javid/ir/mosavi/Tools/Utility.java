package developer.javid.ir.mosavi.Tools;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import developer.javid.ir.mosavi.BuildConfig;
import developer.javid.ir.mosavi.R;

/**
 * Developed by javidDeveloper.ir  *
 */
public class Utility {
    public static String convertNumbersToPersian(String text) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (((int) c) >= 48 && ((int) c) < 58) {
                c = (char) ((c - '0') + 0x0660);
            }
            result.append(c);
        }
        return result.toString();
    }

    public static void copyToClipBoard(Context context, Object object) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("", String.valueOf(object));
        clipboard.setPrimaryClip(clip);
        Toast.makeText(context, "coped !!", Toast.LENGTH_SHORT).show();
    }

    public static Boolean getPermission() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int result = ContextCompat.checkSelfPermission(ContextModel.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(ContextModel.getCurrentActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE}, 101);
                return false;
            }
        }
        return true;
    }


    public static void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) ContextModel.getCurrentActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = ContextModel.getCurrentActivity().getCurrentFocus();
        if (view == null) {
            view = new View(ContextModel.getCurrentActivity());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void hideKeyboard_new() {
        ContextModel.getCurrentActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static String versionName() {
      return   convertNumbersToPersian("نسخه : "+ BuildConfig.VERSION_NAME );
    }


    public static String getNumberItemList(List<Object> list) {
        int size = list.size();
        return " تعداد : " + convertNumbersToPersian(String.valueOf(size)) + " آیتم ";
    }


    public static List<Object> removeDuplicateList(List<Object> list) {
        List<Object> newList = new ArrayList<>();
        for (Object event : list) {
            boolean isFound = false;
            for (Object e : newList) {
//                if (e.getNumber().equals(event.getNumber())) {
                isFound = true;
//                    break;
//                }
            }
            if (!isFound) newList.add(event);
        }
        return newList;

    }

    public static void sendEmail() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{ContextModel.getContext().getString(R.string.my_email)});
        i.putExtra(Intent.EXTRA_SUBJECT, ContextModel.getContext().getString(R.string.send_email_from));
        i.putExtra(Intent.EXTRA_TEXT, "");
        try {
            ContextModel.getCurrentActivity().startActivity(Intent.createChooser(i, ContextModel.getContext().getString(R.string.send_email_with)));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ContextModel.getCurrentActivity(), R.string.email_not_found, Toast.LENGTH_SHORT).show();
        }
    }

    public static String formatNumbers(double number) {
        String s;
        int fPoint; //position of floating in number string
        if (isInteger(number)) {
            s = (long) number + "";
            fPoint = s.length();
        } else {
            s = number + "";
            fPoint = s.lastIndexOf('.');
        }
        StringBuilder formatted = new StringBuilder(s);
        for (int i = fPoint - 3; i > 0; i -= 3) {
            formatted.insert(i, ',');
        }

        return formatted.toString();
    }

    public static boolean isInteger(double number) {
        return (Math.floor(number)) == number;
    }


    @SuppressLint("MissingPermission")
    public static void callNumber(Context context, String phoneNo) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + phoneNo));
//            Utility.getPermissionContactAndSms();
            context.startActivity(intent);
        } catch (Exception ex) {
            Toast.makeText(ContextModel.getContext(), ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    public static String newLine(String number) {
        String t = number.trim();
//        t = t.replace(".", ". \n");
        t = t.replace(":", ": \n");
        t = t.replace("\n", "\n");
        return t;
    }
}
