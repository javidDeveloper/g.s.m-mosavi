//package developer.javid.ir.mosavi.Manager;
//
//import com.s.javid.numbergenerator.Model.MessageList;
//import com.s.javid.numbergenerator.Model.StatusAll;
//import com.s.javid.numbergenerator.Model.VersionControl;
//import com.s.javid.numbergenerator.Model.ViewMessage;
//import com.s.javid.numbergenerator.Network.RestConnection;
//
//import java.util.Observable;
//import java.util.Observer;
//
///**
//* Developed by javidDeveloper.ir
// * Project : Weather_final-master
// */
//public class NetworkManager extends Observable implements Observer {
//    public static NetworkManager instance;
//
//
//    public NetworkManager() {
//        RestConnection.getInstance().addObserver(this);
//    }
//
//    public static NetworkManager getInstance() {
//        if (instance == null)
//            instance = new NetworkManager();
//        return instance;
//    }
//
//    public void callMessages() {
//        RestConnection.getInstance().callMessages();
//    }
//
//    public void callVersion() {
//        RestConnection.getInstance().callVersion();
//    }
//
//    public void addViewMessage(ViewMessage viewMessage) {
//        RestConnection.getInstance().addViewMessage(viewMessage);
//    }
//
//    @Override
//    public void update(Observable observable, Object o) {
//        if (observable instanceof RestConnection) {
//            if (o instanceof MessageList) {
//                setChanged();
//                notifyObservers(o);
//            } else if (o instanceof VersionControl) {
//                setChanged();
//                notifyObservers(o);
//            } else if (o instanceof StatusAll) {
//                setChanged();
//                notifyObservers(o);
//            }
//        }
//    }
//}
