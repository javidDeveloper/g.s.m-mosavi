package developer.javid.ir.mosavi.Fragmnets;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import developer.javid.ir.mosavi.MainActivity;
import developer.javid.ir.mosavi.R;
import developer.javid.ir.mosavi.Tools.Utility;
import developer.javid.ir.mosavi.Widgets.CustomImageView;
import developer.javid.ir.mosavi.Widgets.CustomTextView;

/**
 * Developed by javidDeveloper.ir  *
 */
public class AboutMeFragment extends Fragment {
    private View rootView;
    private CustomImageView imageViewMenu;
    private FloatingActionButton fab;
    private CustomTextView t1;
    private CustomTextView tv_my_site;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_about_me, container, false);
        initViews();
        initText();
        imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getContext()).getDrawer().openDrawer(Gravity.RIGHT);
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CallMeFragment fragment = new CallMeFragment();
                ((MainActivity)getContext()).getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_continer, fragment)
                .commit();
                ((MainActivity)getActivity()).getNavigationView().setCheckedItem(R.id.nav_call_me);
            }
        });
        tv_my_site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.url_my_site)));
                startActivity(browserIntent);
            }
        });
        return rootView;
    }

    private void initText() {
        t1.setText(Utility.newLine(getResources().getString(R.string.info_me)));
    }


    private void initViews() {
        imageViewMenu = rootView.findViewById(R.id.imageView_menu);
        fab = rootView.findViewById(R.id.fab);
        t1 = rootView.findViewById(R.id.t1);
        tv_my_site = rootView.findViewById(R.id.tv_my_site);
    }
}
