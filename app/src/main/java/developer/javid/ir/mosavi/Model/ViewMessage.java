package developer.javid.ir.mosavi.Model;

/**
* Developed by javidDeveloper.ir  *   */
public class ViewMessage {
    private String messageID;
    private String imeiCode;

    public ViewMessage() {
    }

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public String getImeiCode() {
        return imeiCode;
    }

    public void setImeiCode(String imeiCode) {
        this.imeiCode = imeiCode;
    }
}
