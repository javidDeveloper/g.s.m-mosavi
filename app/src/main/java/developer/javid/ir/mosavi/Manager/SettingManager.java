package developer.javid.ir.mosavi.Manager;

import android.preference.Preference;
import android.preference.PreferenceManager;

import com.orhanobut.hawk.Hawk;

import developer.javid.ir.mosavi.Tools.ContextModel;

/**
 * Developed by javid
 * Project : mosavi GSM
 */
public class SettingManager {
    public static final String UNIT_PRICE = "unite.price";
    private static final String FIRST_SHOW_NAVIGATION ="first.show.navigation" ;
    public static final String RIAL = "rial";
    public static final String TOMAN = "toman";
    private static PreferenceManager preference;
    private static SettingManager instance;
    private boolean firstRun;

    public static  SettingManager getInstance() {
        if (instance == null)
            instance = new SettingManager();
        preference.getDefaultSharedPreferences(ContextModel.getContext());
        return instance;
    }

    public boolean getFirstRun() {
        return preference.getDefaultSharedPreferences(ContextModel.getContext()).getBoolean(FIRST_SHOW_NAVIGATION, true) ;

    }

    public void setFirstRun(boolean firstRun) {
        preference.getDefaultSharedPreferences(ContextModel.getContext()).edit().putBoolean(FIRST_SHOW_NAVIGATION, firstRun).apply();
    }




    public String getUnitPrice() {
        return Hawk.get(UNIT_PRICE, TOMAN);
    }

    public void setUnitPrice(String unitPrice) {
        Hawk.put(UNIT_PRICE, unitPrice);
    }
}
