//package developer.javid.ir.mosavi.Fragmnets;
//
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.airbnb.lottie.LottieAnimationView;
//import developer.javid.ir.mosavi.MainActivity;
//import developer.javid.ir.mosavi.R;
//import developer.javid.ir.mosavi.Tools.Utility;
//import developer.javid.ir.mosavi.Widgets.CustomImageView;
//
//
///**
//* Developed by javidDeveloper.ir  *   */
//public class FirstFragment extends Fragment {
//    private View rootView;
//    private LottieAnimationView lotte;
//    private CustomImageView imageViewMenu;
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        rootView = inflater.inflate(R.layout.fragment_first, container, false);
//        initViews();
//        imageViewMenu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Utility.hideKeyboard();
//                ((MainActivity) getContext()).getDrawer().openDrawer(Gravity.RIGHT);
//            }
//        });
//
//        return rootView;
//    }

//    private void initFastAccess() {
//        if (fastAccesses.size() == 0)
//            createFragmentList();
//        adapter = new FastAccessAdapter(fastAccesses, getContext());
//        layoutManager = new LinearLayoutManager(getContext(), LinearLayout.HORIZONTAL, true);
//        recyclerViewFastAccess.setLayoutManager(layoutManager);
//        recyclerViewFastAccess.setAdapter(adapter);
//        adapter.setOnItemClickListener(new OnItemClickListener() {
//            @Override
//            public void onItemClicked(int position) {
//                if (adapter == null)
//                    return;
//                if (adapter.getList().get(position).getPageName().equals(FastAccess.ABOUT_ME_PAGE)) {
//                    AboutMeFragment fragment = new AboutMeFragment();
//                    android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                    fragmentTransaction.add(R.id.frame_continer, fragment);
//                    fragmentTransaction.addToBackStack("AboutMeFragment");
//                    fragmentTransaction.commit();
//                }
//                if (adapter.getList().get(position).getPageName().equals(FastAccess.EMAIL_PAGE)) {
//                    Utility.sendEmail();
//                }
//                if (adapter.getList().get(position).getPageName().equals(FastAccess.SETTING_PAGE)) {
//                    SettingFragment fragment = new SettingFragment();
//                    android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                    fragmentTransaction.add(R.id.frame_continer, fragment);
//                    fragmentTransaction.addToBackStack("SettingFragment");
//                    fragmentTransaction.commit();
//                }
//                if (adapter.getList().get(position).getPageName().equals(FastAccess.SEND_LIST_PAGE)) {
//                    SendSmsListFragment fragment = new SendSmsListFragment();
//                    android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                    fragmentTransaction.add(R.id.frame_continer, fragment);
//                    fragmentTransaction.addToBackStack("SendSmsListFragment");
//                    fragmentTransaction.commit();
//                }
//                if (adapter.getList().get(position).getPageName().equals(FastAccess.NUMBER_GENERATOR_PAGE)) {
//                    GenerateAndSaveFragment fragment = new GenerateAndSaveFragment();
//                    android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                    fragmentTransaction.add(R.id.frame_continer, fragment);
//                    fragmentTransaction.addToBackStack("GenerateAndSaveFragment");
//                    fragmentTransaction.commit();
//                }
//                if (adapter.getList().get(position).getPageName().equals(FastAccess.SEND_SMS_PAGE)) {
//                    ContactFragment fragment = new ContactFragment();
//                    android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                    fragmentTransaction.add(R.id.frame_continer, fragment);
//                    fragmentTransaction.addToBackStack("ContactFragment");
//                    fragmentTransaction.commit();
//                }
//            }
//        });
//
//    }

//    private void createFragmentList() {
//        FastAccess sendSms = new FastAccess();
//        sendSms.setAvatar(getResources().getDrawable(R.drawable.ic_sms));
//        sendSms.setTitle(getResources().getString(R.string.profesional_contact_sms));
//        sendSms.setPageName(FastAccess.SEND_SMS_PAGE);
//        fastAccesses.add(sendSms);
//
//        FastAccess numberGenerator = new FastAccess();
//        numberGenerator.setAvatar(getResources().getDrawable(R.drawable.ic_main_icon));
//        numberGenerator.setTitle(getResources().getString(R.string.number_generator));
//        numberGenerator.setPageName(FastAccess.NUMBER_GENERATOR_PAGE);
//        fastAccesses.add(numberGenerator);
//
//        FastAccess sendList = new FastAccess();
//        sendList.setAvatar(getResources().getDrawable(R.drawable.ic_save_list));
//        sendList.setTitle(getResources().getString(R.string.send_lists));
//        sendList.setPageName(FastAccess.SEND_LIST_PAGE);
//        fastAccesses.add(sendList);
//
//        FastAccess setting = new FastAccess();
//        setting.setAvatar(getResources().getDrawable(R.drawable.ic_settings_avatar));
//        setting.setTitle(getResources().getString(R.string.settings));
//        setting.setPageName(FastAccess.SETTING_PAGE);
//        fastAccesses.add(setting);
//
//        FastAccess email = new FastAccess();
//        email.setAvatar(getResources().getDrawable(R.drawable.ic_email_avatar));
//        email.setTitle(getResources().getString(R.string.send_email));
//        email.setPageName(FastAccess.EMAIL_PAGE);
//        fastAccesses.add(email);
//
//        FastAccess aboutMe = new FastAccess();
//        aboutMe.setAvatar(getResources().getDrawable(R.drawable.ic_support));
//        aboutMe.setTitle(getResources().getString(R.string.about_me));
//        aboutMe.setPageName(FastAccess.ABOUT_ME_PAGE);
//        fastAccesses.add(aboutMe);
//    }

//
//    private void initViews() {
//        imageViewMenu = rootView.findViewById(R.id.imageView_menu);
////        lotte = rootView.findViewById(R.id.lotte);
//    }
//}
