package developer.javid.ir.mosavi.Fragmnets;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import developer.javid.ir.mosavi.MainActivity;
import developer.javid.ir.mosavi.Manager.SettingManager;
import developer.javid.ir.mosavi.R;
import developer.javid.ir.mosavi.Widgets.CustomButton;
import developer.javid.ir.mosavi.Widgets.CustomEditText;
import developer.javid.ir.mosavi.Widgets.CustomImageView;
import developer.javid.ir.mosavi.Widgets.CustomTextView;


/**
 * Developed by javidDeveloper.ir  *
 */
public class SettingFragment extends Fragment {
    private View rootView;
    private CustomImageView imageViewMenu;
    private CustomButton buttonSaveSetting;
    private RadioGroup gpPriceUnit;
    private int priceUnit;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_setting, container, false);
        initViews();
        imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getContext()).getDrawer().openDrawer(Gravity.RIGHT);
            }
        });
        if (SettingManager.getInstance().getUnitPrice().equals(SettingManager.RIAL)) {
            gpPriceUnit.check(R.id.rd_rial);
        } else if (SettingManager.getInstance().getUnitPrice().equals(SettingManager.TOMAN)) {
            gpPriceUnit.check(R.id.rd_toman);
        }
        gpPriceUnit.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rd_rial:
                        SettingManager.getInstance().setUnitPrice(SettingManager.RIAL);
                        break;
                    case R.id.rd_toman:
                        SettingManager.getInstance().setUnitPrice(SettingManager.TOMAN);
                        break;
                }
            }
        });


        buttonSaveSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
        return rootView;
    }


    private void initViews() {
        imageViewMenu = rootView.findViewById(R.id.imageView_menu);
        buttonSaveSetting = rootView.findViewById(R.id.button_save_setting);
        gpPriceUnit = rootView.findViewById(R.id.gp_price_unit);
        priceUnit = gpPriceUnit.getCheckedRadioButtonId();
    }
}
