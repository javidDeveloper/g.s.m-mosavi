package developer.javid.ir.mosavi.Fragmnets;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import developer.javid.ir.mosavi.MainActivity;
import developer.javid.ir.mosavi.Manager.DimensionsManager;
import developer.javid.ir.mosavi.Manager.SettingManager;
import developer.javid.ir.mosavi.R;
import developer.javid.ir.mosavi.Tools.ContextModel;
import developer.javid.ir.mosavi.Tools.Utility;
import developer.javid.ir.mosavi.Widgets.CustomImageView;
import developer.javid.ir.mosavi.Widgets.CustomTextView;

/**
 * Developed by javid
 */
public class CalculateFragment extends Fragment {
    private View rootView;
    private Button fabCalc;
    private Button buttonReset;
    private CustomImageView imageViewMenu;
    private EditText editTextPrice;
    private EditText editTextMainWidth;
    private EditText editTextMainHeight;
    private CustomTextView textViewPriceUnit;
    private CustomTextView textViewTotalPrice;
    private CustomTextView textViewHeightFrame;
    private CustomTextView textViewWidthFrame;
    private CustomTextView textViewMovable;
    private CustomTextView textViewHeightNet;
    private CustomTextView textViewStepNet;
    private CustomTextView textViewThread;
    private CustomTextView textViewTotalSequerMeter;
    private CustomTextView textViewErrorDimensions;
    private CustomTextView textViewErrorPrice;
    private long price = 0;
    private double mainWidth = 0;
    private double mainHeight = 0;
    private String txt = null;

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_calculate, container, false);
        initViews();
        applySettings();
        Utility.hideKeyboard_new();
        fabCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textViewErrorDimensions.setVisibility(View.INVISIBLE);
                textViewErrorPrice.setVisibility(View.INVISIBLE);
                if (editTextMainHeight.getText().toString().equals("") || editTextMainWidth.getText().toString().equals("")) {
                    textViewErrorDimensions.setVisibility(View.VISIBLE);
                    return;
                }
                if (editTextPrice.getText().toString().equals("")) {
                    textViewErrorPrice.setVisibility(View.VISIBLE);
                    return;
                }
                if (Double.parseDouble(editTextPrice.getText().toString()) == 0) {
                    textViewErrorPrice.setVisibility(View.VISIBLE);
                    return;
                }
                Utility.hideKeyboard();
                price = Long.valueOf(editTextPrice.getText().toString().trim());
                mainWidth = Double.valueOf(editTextMainWidth.getText().toString().trim());
                mainHeight = Double.valueOf(editTextMainHeight.getText().toString().trim());
                textViewTotalPrice.setText(Utility.convertNumbersToPersian(DimensionsManager.getInstance().totalPrice(mainHeight, mainWidth, price)));
                textViewHeightFrame.setText(Utility.convertNumbersToPersian(String.valueOf(DimensionsManager.getInstance().heightFrame(mainHeight))));
                textViewWidthFrame.setText(Utility.convertNumbersToPersian(String.valueOf(DimensionsManager.getInstance().widthFrame(mainWidth))));
                textViewMovable.setText(Utility.convertNumbersToPersian(String.valueOf(DimensionsManager.getInstance().movable(mainHeight))));
                textViewHeightNet.setText(Utility.convertNumbersToPersian(String.valueOf(DimensionsManager.getInstance().heightNet(mainHeight))));
                textViewStepNet.setText(Utility.convertNumbersToPersian(String.valueOf(DimensionsManager.getInstance().stepNet(mainWidth))));
                textViewThread.setText(Utility.convertNumbersToPersian(String.valueOf(DimensionsManager.getInstance().thread(mainHeight, mainWidth))));
                textViewTotalSequerMeter.setText(Utility.convertNumbersToPersian(String.valueOf(DimensionsManager.getInstance().totalSquareMeter(mainHeight, mainWidth))));
            }
        });
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                price = 0;
                mainWidth = 0;
                mainHeight = 0;
                editTextPrice.setText("");
                editTextMainWidth.setText("");
                editTextMainHeight.setText("");
                textViewTotalPrice.setText("---");
                textViewHeightFrame.setText("---");
                textViewWidthFrame.setText("---");
                textViewMovable.setText("---");
                textViewHeightNet.setText("---");
                textViewStepNet.setText("---");
                textViewThread.setText("---");
                textViewTotalSequerMeter.setText("---");
                textViewErrorDimensions.setVisibility(View.INVISIBLE);
                textViewErrorPrice.setVisibility(View.INVISIBLE);

            }
        });
        imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.hideKeyboard();
                ((MainActivity) getContext()).getDrawer().openDrawer(Gravity.RIGHT);
            }
        });

        return rootView;
    }

    private void applySettings() {
        if (SettingManager.getInstance().getUnitPrice().equals(SettingManager.TOMAN)) {
            textViewPriceUnit.setText(getContext().getResources().getString(R.string.toman));
        } else if (SettingManager.getInstance().getUnitPrice().equals(SettingManager.RIAL)) {
            textViewPriceUnit.setText(getContext().getResources().getString(R.string.rial));
        }
        }

    private void initViews() {
        fabCalc = rootView.findViewById(R.id.fab_calc);
        buttonReset = rootView.findViewById(R.id.btn_reset);
        imageViewMenu = rootView.findViewById(R.id.imageView_menu);
        editTextPrice = rootView.findViewById(R.id.editText_price);
        editTextMainWidth = rootView.findViewById(R.id.editText_main_width);
        editTextMainHeight = rootView.findViewById(R.id.editText_main_height);
        textViewPriceUnit = rootView.findViewById(R.id.tv_price_unit);
        textViewTotalPrice = rootView.findViewById(R.id.textView_total_price);
        textViewHeightFrame = rootView.findViewById(R.id.textView_height_frame);
        textViewWidthFrame = rootView.findViewById(R.id.textView_width_frame);
        textViewMovable = rootView.findViewById(R.id.textView_movable);
        textViewHeightNet = rootView.findViewById(R.id.textView_height_net);
        textViewStepNet = rootView.findViewById(R.id.textView_step_net);
        textViewThread = rootView.findViewById(R.id.textView_thread);
        textViewTotalSequerMeter = rootView.findViewById(R.id.textView_total_sequer_meter);
        textViewErrorDimensions = rootView.findViewById(R.id.textView_error_dimensions);
        textViewErrorPrice = rootView.findViewById(R.id.textView_error_price);
    }
}
